@extends('layouts.admin')
 @section('title','Détail d\'un produit')
 @section('content')
 <a href="{{route('produits.index')}}">Retourner à la liste des produits</a>
    <h3>ID : {{$produit->id}}</h3>
    <p>Designation : {{$produit->designation}}</p>
    <p>Prix unitaire : {{$produit->prix_u}}</p>
    <p>Quantite_stock : {{$produit->quantite_stock}}</p>
    <p>Categorie : {{$produit->categorie->designation}}</p>
    <div>
        <img src="{{asset('storage/'.$produit->image)}}" alt="photo_produit">
    </div>
    
@endsection