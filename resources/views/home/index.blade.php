 @extends('layouts.admin')
 @section('title','Gestion des categories')
 @section('content')
     
    <h1>Catalogue</h1>
    <div class="catalogue py-3" style="width:80%;">
   
   @foreach ($produits as $item)
       
       <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="{{asset('storage/'.$item->image)}}" alt="" style="height:300px;">
            <div class="card-body">
                <h5 class="card-title">{{$item->designation}}</h5>
                <p class="card-text">Prix : {{$item->prix_u}} MAD</p>
                @if ($item->quantite_stock==0)
                    <p>En repture de stock</p>
                @else
                <p>En stock : {{$item->quantite_stock}}</p>
                <form action="{{route('home.add',["id"=>$item->id])}}" method='POST'>
                @csrf
                    <label for="qte">Quantite</label>
                    <input type="number" name="qte" id="qte" min="1" max="{{$item->quantite_stock}}">
                    <input type="submit" value="Acheter" class="btn btn-primary">
                </form>
                @endif
            </div>
        </div>
 
   @endforeach
     
    </div>

@endsection