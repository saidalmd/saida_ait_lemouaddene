@extends('layouts.admin')
@section('title','Gestion des categories')
@section('content')
<form action="{{route('clients.store')}}" method="post">
    @csrf
    <div>
        <label for="nom">Nom</label>
        <input type="text" id="nom" name="nom"/>
    </div>
    <div>
        <label for="prenom">Prenom</label>
        <input type="text" id="prenom" name="prenom"/>
    </div>
    <div>
        <label for="tele">Tele</label>
        <input type="text" id="tele" name="tele"/>
    </div>
    <div>
        <label for="ville">Ville</label>
        <input type="text" id="ville" name="ville"/>
    </div>
    <div>
        <label for="adresse">Adresse</label>
        <input type="text" id="adresse" name="adresse"/>
    </div>
    
    <div>
        <input type="submit" value="Enregistrer">
    </div>
</form>
@endsection