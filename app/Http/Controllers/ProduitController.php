<?php

namespace App\Http\Controllers;
use App\Models\Produit;
use App\Models\Categorie;

use Illuminate\Http\Request;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        //
            $list_categories=Categorie::select('id','designation')->get();
            $des=$request->query('designation');
            $prix_min=$request->query('prix_u_min');
            $prix_max=$request->query('prix_u_max');
            $quantite=$request->query('quantite_stock');
            $categorie_id=$request->query('categorie_id');

            $p=Produit::query();
            if($des){
                $p->where('designation','like','%'.$des.'%');
            }
            if($prix_min){
                $p->where('prix_u','>',$prix_min);
            }
            if($prix_max){
                $p->where('prix_u','<',$prix_max);

            }
            if($quantite){
                $p->where('quantite_stock','>',$quantite);
            }
            if($categorie_id){
                $p->where('categorie_id','=',$categorie_id);
            }
            $produits=$p->paginate(5);
            $produits->appends([
                'designation'=>$des,
                'prix_u_min'=>$prix_min,
                'prix_u_max'=>$prix_max,
                'quantite_stock'=>$quantite,
                'categorie_id'=>$categorie_id
            ]);
            return view('produits.index',compact('produits','list_categories'));
            
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $list_categories=Categorie::select('id','designation')->get();
        return view('produits.create',compact('list_categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $validatedData=$request->validate(
            [
                'designation'=>'required|unique:produits,designation',
                'prix_u'=>'required',
                'quantite_stock'=>'required',
                'categorie_id'=>'required',
                'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:80000',
            ]
            );
            if($request->hasFile('image')){
                $imagePath=$request->file('image')->store('products/images','public');
                $validatedData['image']=$imagePath;
            }
        Produit::create($validatedData);
        return redirect()->route('produits.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
        $produit=Produit::find($id)->load('categorie');
        if($produit){
            return view('produits.show',compact('produit'));
        }
        else {
            abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $produit=Produit::find($id);
        if($produit){
            $list_categories=Categorie::select('id','designation')->get();
            return view('produits.edit',compact('produit','list_categories'));
        }
        else {
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $validatedData=$request->validate(
            [
                'designation'=>'required|unique:produits,designation,'.$id,
                'prix_u'=>'required',
                'quantite_stock'=>'required',
                'categorie_id'=>'required',
                'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]
            );
            if($request->hasFile('image')){
                $imagePath=$request->file('image')->store('products/images','public');
                $validatedData['image']=$imagePath;
            }
        $produit=Produit::find($id);
        $produit->update($validatedData);
        return redirect()->route('produits.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        Produit::destroy($id);
        return redirect()->route('produits.index');
    }
}
