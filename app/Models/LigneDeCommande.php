<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relation\Pivot;

class LigneDeCommande extends Pivot
{
    protected $table='ligne_de_commande';
    protected $fillable=['quantite'];
}
