<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;
    protected $fillable=['datetime','client_id','etat_id'];

    public function client(){
        return $this->belongsTo(Client::class);
    }

    public function etat(){
        return $this->belongsTo(Etat::class);
    }

    public function produits(){
        return $this->belongsToMany(Produit::class)
                    ->using(LigneDeCommande::class)
                    ->withPivot('quantite');
    }
}
